import React from 'react'

import s from './StatBlock.module.css'

const StatBlock = ({ title, count }) => {
  return (
    <div className={s.block}>
      <p className={s.count}>{count}</p>
      <h2 className={s.title}>{title}</h2>
    </div>
  )
}

export default StatBlock
