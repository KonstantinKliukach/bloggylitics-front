import React from 'react'

import Avatar from '../Avatar'
import Bookmark from '../Bookmark'
import Title from '../Title'
import Button from '../Button'
import StatBlock from '../StatBlock'

import s from './Card.module.css'

const Card = ( {user} ) => {

  const {profile, title, image, stats} = user

  const onClick = () =>{
    /* do something */
  }
  return (
    <div className={s.card} >
      <div className={s.info}>
        <Avatar image={image}/>
        <Title profile={profile} title={title} />
        <Bookmark />
        <Button text="Перейти" onClick={onClick} />
      </div>
      <div className={s.stats}>
        {stats.map(stat => {
          return(
            <StatBlock title={stat.title} count={stat.count} />
          )
        })}
      </div>
    </div>
  )
}

export default Card