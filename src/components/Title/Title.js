import React from 'react'

import s from './Title.module.css'


const Title = ({profile, title}) => {
  return (
    <a className={s.title} href={profile}>{title}</a>
  )
}

export default Title