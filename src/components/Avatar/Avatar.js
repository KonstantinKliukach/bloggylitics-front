import React from 'react'

import s from './Avatar.module.css'

const Avatar = ({image}) => {
  return (
    <div className={s.ava} style={{backgroundImage: `url(${image})`}}/>
  )
}

export default Avatar