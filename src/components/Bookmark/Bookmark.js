import React from 'react'

import s from './Bookmark.module.css'

import bookmark from '../../assets/bookmark.png'

const Bookmark = ({onClick}) => {
  return (
    <div className={s.bookmark} style={{backgroundImage: `url(${bookmark})`}}/>
  )
}

export default Bookmark