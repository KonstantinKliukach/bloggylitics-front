import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import Card from '../src/components/Card';
import avaFile from '../src/assets/back.png'

const user = {
  image: avaFile,
  title: "@romankorneev",
  profile: "www.google.com",
  stats: [
    {title: 'ER', count: '5,23%'},
    {title: 'Лайки', count: '55М'},
    {title: 'Подписки', count: '23М'},
    {title: 'Вовлеченные подписчики', count: '1,6М'}
  ]
}

storiesOf('Card', module)
  .add('default', () => <Card user={user}/>)